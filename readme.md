# DummyTel
***
DummyTel là một ***chương trình theo dõi cuộc gọi và tính phí dựa trên thời gian bắt đầu cuộc gọi và thời 
lượng của cuộc gọi tính từ lúc bắt đầu***. Sẽ có những ***mức giảm giá nhất định*** dựa trên thời gian bắt đầu 
cuộc gọi và thời lượng cuộc gọi.

## Edit Configurations
***
### DummyTel Build Config
<p>
    <img src="https://scontent.fhan5-2.fna.fbcdn.net/v/t1.15752-9/331079850_1227846308118180_4209820171664595099_n.png?_nc_cat=104&ccb=1-7&_nc_sid=ae9488&_nc_ohc=wzU-YMYa1soAX_bPaMA&_nc_ht=scontent.fhan5-2.fna&oh=03_AdRjf0US-7PdF44ZWS5RzCE97gjbVSVU3QfVcPKD3EsLBw&oe=64169E58"/>
</p>

### DummyTel Test Config
<p>
    <img src="https://scontent.fhan5-11.fna.fbcdn.net/v/t1.15752-9/331410001_608963937735378_2890198361322973428_n.png?_nc_cat=103&ccb=1-7&_nc_sid=ae9488&_nc_ohc=xscmfyQx204AX97Rw-B&_nc_ht=scontent.fhan5-11.fna&oh=03_AdSS_ENO4qx7tIKXQYeEgecOjIhDxJG8ETrrrCiebmkfQg&oe=641694E6"/>
</p>

## Build Gradle
***
Trên thanh công cụ chọn config của DummyTel Build Config và ấn nút *Play* hoặc sử dụng tổ hợp phím Shift+F10 nếu dùng keymap mặc định của Jetbrains. Kết quả hiển thị khi thành công:

<p>
    <img src="https://scontent.fhan5-8.fna.fbcdn.net/v/t1.15752-9/331109378_5908872862533393_3906746089355018369_n.png?_nc_cat=108&ccb=1-7&_nc_sid=ae9488&_nc_ohc=UYMyTGfXr3IAX-I1aXi&tn=EFUGJSrNibt_VjfA&_nc_ht=scontent.fhan5-8.fna&oh=03_AdRIBW-xJFmOPVpvt60vfFwDGFsVo5-gUe90QwVNdxa0yA&oe=641694AC"/>
</p>

## Usage
***
### 1. Testing
#### 1.1 Run test
Trên thanh công cụ chọn config của DummyTel  Config và ấn nút *Play* hoặc sử dụng tổ hợp phím Shift+F10 nếu dùng keymap mặc định của Jetbrains
 
Tests
#### 1.2 Test result
Kết quả sau khi chạy các bộ test thành công
<p>
    <img src="https://scontent.fhan5-2.fna.fbcdn.net/v/t1.15752-9/331201021_910083993368159_8545458335682955887_n.png?_nc_cat=104&ccb=1-7&_nc_sid=ae9488&_nc_ohc=vQjXmk0HFK8AX8WA9vI&tn=EFUGJSrNibt_VjfA&_nc_ht=scontent.fhan5-2.fna&oh=03_AdTL8u26MM9n5PoQFTNHMmo7KwcFZx_n63md1o3aLIj4nQ&oe=64169CEF"/>
</p>

#### 1.3 Custom test case 
Trong ví dụ đã có mẫu test case cho 3 trường hợp cơ bản dựa trên đó có thể dễ dàng thêm các bộ test case theo ý muốn.

<p>
    <img src="https://scontent.fhan5-10.fna.fbcdn.net/v/t1.15752-9/331137633_3163170133974150_7458009100738643067_n.png?_nc_cat=101&ccb=1-7&_nc_sid=ae9488&_nc_ohc=IPjezeUWPAwAX9NudsT&tn=EFUGJSrNibt_VjfA&_nc_ht=scontent.fhan5-10.fna&oh=03_AdRXAWQaVLa0KmybUAl57Hwzgq1FI572W-hniEczVGOPEw&oe=64168343"/>
</p>