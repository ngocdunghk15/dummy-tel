package modules.dummyTel;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * The DummyTel class is used to track and manage a phone call. It includes methods to calculate the total net cost of a
 * phone call, as well as methods to set the start time and call duration time. The class uses a number of constants to
 * determine the cost and discount rates for the phone call, as well as to determine the lower and upper bounds of the
 * discount range based on the start time.
 *
 * @author ngocdung-20020379
 * @version 1.0.0
 * @since 14-02-2023
 */
public class DummyTel {
  /**
   * Static value {@value #DUMMY_TEL_TAX}. The tax rate for the phone call.
   */
  private final static double DUMMY_TEL_TAX = 0.05;
  /**
   * Static value {@value #DUMMY_TEL_COST_PER_MINUTE}. The cost per minute for the phone call.
   */
  private final static double DUMMY_TEL_COST_PER_MINUTE = 0.5;
  /**
   * Static value {@value #DISCOUNT_START_TIME_BETWEEN_DISCOUNT_RANGE_TIME}. The discount rate for calls that start during the discount range.
   */
  private final static double DISCOUNT_START_TIME_BETWEEN_DISCOUNT_RANGE_TIME = 0;
  /**
   * Static value {@value #DISCOUNT_START_TIME_OUTSIDE_DISCOUNT_RANGE_TIME}. The discount rate for calls that start outside the discount range.
   */
  private final static double DISCOUNT_START_TIME_OUTSIDE_DISCOUNT_RANGE_TIME = 0.5;
  /**
   * Static value {@value #MINIMUM_VALUE_OF_MINUTE_TO_GET_DISCOUNT}. The minimum call duration time required to get a discount.
   */
  private final static double MINIMUM_VALUE_OF_MINUTE_TO_GET_DISCOUNT = 60;
  /**
   * Static value {@value #DISCOUNT_CALL_DURATION_LONGER_THAN_60M}. The discount rate for calls with a duration longer or equal than 60 minutes.
   */
  private final static double DISCOUNT_CALL_DURATION_LONGER_THAN_60M = 0.15;
  /**
   * Static value {@value #DISCOUNT_CALL_DURATION_LESS_THAN_60M}. The discount rate for calls with a duration less than to 60 minutes.
   */
  private final static double DISCOUNT_CALL_DURATION_LESS_THAN_60M = 0;
  /**
   * The lower bound of the discount range based on the start time.
   */
  private final static LocalTime LOWER_BOUND_TIME_VALUE = LocalTime.of(8, 0);
  /**
   * The upper bound of the discount range based on the start time.
   */
  private final static LocalTime UPPER_BOUND_TIME_VALUE = LocalTime.of(18, 0);
  /**
   * The start time of the phone call.
   */
  private LocalTime startTime;
  /**
   * The duration of the phone call in minutes.
   */
  private int callDurationTime;

  /**
   * Creates a new DummyTel object with the current time as the start time and a call duration time of 0.
   */
  public DummyTel() {
    this.startTime = LocalTime.now();
    this.callDurationTime = 0;
  }

  /**
   * Sets the start time of the phone call.
   *
   * @param startTime The start time of the phone call.
   */
  public void setStartTime(LocalTime startTime) {
    this.startTime = startTime;
  }

  /**
   * Sets the call duration time of the phone call.
   *
   * @param callDurationTime The call duration time of the phone call in minutes.
   */
  public void setCallDurationTime(int callDurationTime) {
    this.callDurationTime = callDurationTime;
  }

  /**
   * Calculates the discount rate for the phone call based on the start time.
   *
   * @return The discount rate for the phone call based on the start time.
   */
  private double calculateDiscountBaseOnStartTime() {
    double compareValueWithLowerBoundTime = this.startTime.compareTo(LOWER_BOUND_TIME_VALUE);
    double compareValueWithUpperBoundTime = this.startTime.compareTo(UPPER_BOUND_TIME_VALUE);
    // Notice if the start time is between lower bound time and upper bound time, sum of two compared values is 0 or -1 that reason why I define indexRange variable.
    double indexRange = compareValueWithLowerBoundTime + compareValueWithUpperBoundTime;
    if (indexRange == -1 || indexRange == 0) {
      return DISCOUNT_START_TIME_BETWEEN_DISCOUNT_RANGE_TIME;
    } else {
      return DISCOUNT_START_TIME_OUTSIDE_DISCOUNT_RANGE_TIME;
    }
  }

  /**
   * Calculates the discount rate for the phone call based on the call duration.
   *
   * @return The discount rate for the phone call based on the call duration.
   */
  private double calculateDiscountBaseOnCallDuration() {
    if (this.callDurationTime >= MINIMUM_VALUE_OF_MINUTE_TO_GET_DISCOUNT) {
      return DISCOUNT_CALL_DURATION_LONGER_THAN_60M;
    } else {
      return DISCOUNT_CALL_DURATION_LESS_THAN_60M;
    }
  }

  /**
   * Calculates the cost for the phone call based on the call duration.
   *
   * @return The cost of the phone call calculated based on the call duration.
   */
  private double calculateTotalCostBaseOnCallDurationTime() {
    return this.callDurationTime * DUMMY_TEL_COST_PER_MINUTE;
  }

  /**
   * Format the localtime to pattern hh:mm
   *
   * @param time - time format is LocalTime
   * @return Formatted time with pattern hh:mm
   */
  private String getFormattedDateTime(LocalTime time) {
    return DateTimeFormatter.ofPattern("HH:mm").format(time);
  }

  /**
   * Reads input from the user to set the call duration time, and sets the value of the callDurationTime field.
   * Throws an IOException if the input is not a positive integer.
   */
  private void inputCallDuration() {
    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
    try {
      System.out.println("Start time: " + getFormattedDateTime(this.startTime));
      System.out.println("Please enter call duration ( Number of minutes ): ");
      int callDurationTime = Integer.parseInt(bufferedReader.readLine());
      if (callDurationTime <= 0) {
        throw new IOException("Value of call duration time must be positive integer!");
      }
      setCallDurationTime(callDurationTime);
    } catch (Exception error) {
      error.printStackTrace();
    }
  }

  /**
   * Calculates the total net cost of the call, based on the call duration time, start time, and various discounts.
   *
   * @return The total net cost of the call as a double.
   */
  public double calculateTotalNetCost() {
    double totalNetCost;
    double floorTotalNetCost;
    this.inputCallDuration();
    double totalCostBaseOnCallDurationTime = this.calculateTotalCostBaseOnCallDurationTime();
    double discountBaseOnStartTime = this.calculateDiscountBaseOnStartTime();
    double discountBaseOnCallDuration = this.calculateDiscountBaseOnCallDuration();
    totalNetCost = (totalCostBaseOnCallDurationTime * (1.0 - discountBaseOnStartTime) * (1.0 - discountBaseOnCallDuration)) * (1.0 + DUMMY_TEL_TAX);
    floorTotalNetCost = Math.floor(totalNetCost * 10) / 10;
    return floorTotalNetCost;
  }

  /**
   * Calculates the total net cost of a phone call, given a start time and call duration time,
   * and returns the result as a double. This method is used for testing purposes only.
   *
   * @param startTime The start time of the phone call, as a LocalTime object.
   * @param callDurationTime The duration of the phone call, in minutes.
   * @return The total net cost of the phone call, as a double.
   * @throws IllegalAccessException If the call duration time is less than or equal to zero, an IllegalAccessException
   * is thrown with a message "Value of call duration time must be positive integer!".
   */
  public double testCalculateTotalNetCost(LocalTime startTime, int callDurationTime) throws IllegalAccessException {
    double totalNetCost;
    double floorTotalNetCost;
    if (callDurationTime <= 0) {
      throw new IllegalAccessException("Value of call duration time must be positive integer!");
    }
    this.setStartTime(startTime);
    this.setCallDurationTime(callDurationTime);
    double totalCostBaseOnCallDurationTime = this.calculateTotalCostBaseOnCallDurationTime();
    double discountBaseOnStartTime = this.calculateDiscountBaseOnStartTime();
    double discountBaseOnCallDuration = this.calculateDiscountBaseOnCallDuration();
    totalNetCost = (totalCostBaseOnCallDurationTime * (1.0 - discountBaseOnStartTime) * (1.0 - discountBaseOnCallDuration)) * (1.0 + DUMMY_TEL_TAX);
    floorTotalNetCost = Math.floor(totalNetCost * 10) / 10;
    return floorTotalNetCost;
  }
}
