package modules;

import modules.dummyTel.DummyTel;

import java.io.IOException;
import java.time.LocalTime;
import java.util.Optional;

public class Main {
  public static void main(String[] args) throws IOException {
    System.out.println("Start app...");
    DummyTel newDummyTel = new DummyTel();
    double totalNetCost = newDummyTel.calculateTotalNetCost();
    System.out.println("Result: " + totalNetCost);
  }
}