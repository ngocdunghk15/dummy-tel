package modules.dummyTel;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.*;

class DummyTelTest {

  @Test
  @DisplayName("Calculate total net cost base on start time and call duration time.")
  void calculateCostInCaseStartTimeInsideDiscountTimeRange() {
    DummyTel dummyTel = new DummyTel();
    assertAll(
      () -> assertEquals(13.1, dummyTel.testCalculateTotalNetCost(LocalTime.of(0,0,0),50)),
      () -> assertEquals(22.3, dummyTel.testCalculateTotalNetCost(LocalTime.of(20,0,0),100))
    );
  }

  @Test
  @DisplayName("Calculate total net cost base on start time and call duration time.")
  void calculateCostInCaseStartTimeOutsideDiscountTimeRange() {
    DummyTel dummyTel = new DummyTel();
    assertAll(
      () -> assertEquals(26.2, dummyTel.testCalculateTotalNetCost(LocalTime.of(8,0,0),50)),
      () -> assertEquals(44.6, dummyTel.testCalculateTotalNetCost(LocalTime.of(17,0,0),100))
    );
  }

  @Test
  @DisplayName("Test case: Call duration is negative value.")
  void testInCaseCallDurationIsNegativeValue() {
    DummyTel dummyTel = new DummyTel();
    assertThrows(IllegalAccessException.class,
      () -> {
        dummyTel.testCalculateTotalNetCost(LocalTime.of(0,0,0),-20);
      });
  }
}